<?php namespace Model\Mapper;

interface AuthorMapperInterface
{
    public function fetchById($id);
}