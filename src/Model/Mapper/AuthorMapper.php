<?php
namespace Model\Mapper;
use Library\Database\DatabaseAdapterInterface,
    Model\Author;

class AuthorMapper implements AuthorMapperInterface
{
    protected $entityTable = "authors";

    public function __construct(DatabaseAdapterInterface $adapter) {
        $this->adapter = $adapter;
    }

    public function fetchById($id) {
        $this->adapter->select($this->entityTable,
            array("id" => $id));

        if (!$row = $this->adapter->fetch()) {
            return null;
        }

        return new Author($row["name"], $row["email"]);
    }
}