<?php
use Library\Database\PdoAdapter;

use Model\Mapper\AuthorMapper,
    Model\Proxy\AuthorProxy,
    Model\Author,
    Model\Post;

require_once __DIR__ . '/../src/bootstrap.php';

$adapter = new PdoAdapter('mysql:dbname=my_demo_examples', 'demo', 'demo');

$authorMapper = new AuthorMapper($adapter);

// $author = $authorMapper->fetchById(1);
$author = new AuthorProxy(1, new AuthorMapper($adapter));

$post = new Post('About Men', 
        'Men are born ignorant, not stupid; they are made stupid by education.',
        $author);

echo $post->getTitle() . $post->getContent() . " Quote from: " . 
        $post->getAuthor()->getName();


